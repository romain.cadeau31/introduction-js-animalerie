function exo1() {
    fetch('users.json') 
    .then(function (response) {
        return response.json()
    }).then(function(data) {        
        console.log(data['customers'])
    })
}

function exo2(){
    fetch('users.json').then((response) => {
        response.json().then((data)=> {
            let dataTab = data['customers']
            dataTab.forEach(user => {
                console.log(user['user_title']+' '+user['user_name'])
    
            })
    
        }
        )
    })
}

function exo3(){
    fetch('users.json').then((response) => {
        response.json().then((data)=> {
            let dataTab = data['customers']
            let array = []
            dataTab.forEach(user => {
                let dataHave = user['user_pets']
                dataHave.forEach(anim => {
                    array.push(anim['pet_name'])
                })
    
            })
            array.sort()
            console.log(array.toString())
    
        }
        )
    })
}

function exo4(){
    fetch('users.json').then(response => {
        response.json().then(data => {
            let dataTab = data['customers']
            let array = [];
            dataTab.forEach(user => {
                
                    if(user['user_pets'].length >0){
                    array.push(user['user_title'] + ' ' + user['user_name']);
                    
                }})
                console.log(array.toString());
            })
            
            
    
    })
}

function exo5(){
    fetch('users.json').then(response => 
        response.json().then(data => {
            let mickey = {
                pet_name : 'Mickey',
                pet_type : 'mouse',
                pet_age : 0.1
            }
            let dataTab = data['customers']
            dataTab.forEach(user => {
                user['user_pets'].push(mickey)
            })
            dataTab.forEach(user => {
                console.log(user['user_title']+' '+user['user_name'])
            })
            
        }
        )
    )
}